    <form name="form-editar" id="form-editar" method="post"  role="form">
        @csrf
        <input type="hidden" id="varMenu_id" name="varMenu_id" value="{{$menu->varMenu_id}}">
        <div class="form-group">
            <label>Título</label>
            <input type="text" class="form-control" id="varTitle" name="varTitle" placeholder="nombre del menú" autocomplete="off" required value="{{$menu->varTitle}}" >
            <span class="text-danger" id="varTitle_spam"></span>
        </div>
        <div class="form-group">
            <label>Descripción</label>
            <textarea class="form-control" rows="3" id="varDescription" name="varDescription" placeholder="ingrese descripcion del menú" autocomplete="off">{{$menu->varDescription}}</textarea>
            <span class="text-danger" id="varDescription_spam"></span>
        </div>
        <div class="form-group">
          <label>Link</label>
          <input type="text" class="form-control" id="varLink" name="varLink" placeholder="controller/action" autocomplete="off">
        </div>
        <div class="form-group">
            <label>Padre</label>
            <select class="form-control" id="varFather_id" name="varFather_id" style="width: 100%;" >
              @if(!empty($padre))
                <option value="{{$menu->varFather_id}}" selected>{{$padre->varTitle}}</option>  
              @endif
            </select>
        </div>
        <div class="form-group">
            <label>Icono</label>
            <input type="text" class="form-control" id="varIcon" name="varIcon" placeholder="Icono" autocomplete="off" value="{{$menu->verIcon}}">
        </div>
        <div class="form-group">
            <label>Orden</label>
            <input type="text" class="form-control" id="intOrden" name="intOrden" placeholder="ingrese el orden del menú" autocomplete="off" required value="{{$menu->intOrden}}">
            <span class="text-danger" id="intOrden_spam"></span>
        </div> 
    </form>
    {{-- falta acomodar el formulario para que trabaje de forma generica ademas el token no esta cargando con el modal para el envio por post --}}
    <script>
        $(function () {
        $('#varFather_id').select2({  
        placeholder: "Buscar padre",
        minimumInputLength: 3,  
        language: {
          noResults: function() {
            return "No hay resultado";        
          },
          searching: function() {
            return "Buscando..";
            },
          inputTooShort:function(){
            return 'Ingrese 3 o mas caracteres';
          },
        },
        ajax: { 
            url: BASE_URL+'admin/getparents',
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                searchTerm: params.term // search term
              };
            },
            processResults: function (response) {
              return {
                  results: response
              };
            },
            cache: true
          }
        });
        //$('#title').val('Editar Menú');
        //$("#modal-general").dialog("option","title", "Editar Menú");
        //$("#modal-general").find('.modal-header-title').html('Editar Menú');
        });
    </script>