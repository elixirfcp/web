@extends('layouts.administration.header')
@section('style') 
  <link rel="stylesheet" href="{{ asset('assets/admin/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ asset('assets/admin/select2/dist/css/select2.min.css') }}">
  <style>
    .invalid input:required:invalid {
      background: #BE4C54;
    }
  </style>
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      {{-- <h1>
        Menú
        <small>Version 2.0</small>
      </h1> --}}
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Aves</li>
      </ol>
    </section>
    <div class="messages"></div>
    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->
      <div class="box">
        <div class="box-header">
          <h3 class="box-title">Listado del Menú</h3>
          <button type="button" id="btnagregar" name="btnagregar" class="btn btn-primary pull-right" data-toggle="modal" data-target="#modal-menu">Agregar Nuevo Menú</button>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table id="table-menu" class="table table-bordered table-striped">
            <thead>
            <tr>
              <th>Padre</th>
              <th>Título</th>
              <th>Icono</th>
              <th>Link</th>
              <th>Descripción</th>
              <th>Orden</th>
              <th>Estado</th>
              <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" tabindex="-1" data-toggle="modal" data-backdrop="static" data-keyboard="false" id="modal-menu">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
            <span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title w-100 text-center">Ingreso de Nuevo Menú mmmm otro</h4>
        </div>
        <div class="modal-body">
          <form name="form-menu" id="form-menu" action="{{ route('admin.createmenu')}}" method="post"  role="form">
            <div class="form-group">
              <label>Título</label>
              <input type="text" class="form-control" id="varTitle" name="varTitle" placeholder="nombre del menú" autocomplete="off" required value="{{ old('varTitle') }}" >
              <span class="text-danger" id="varTitle_spam"></span>
            </div>
            <div class="form-group">
              <label>Descripción</label>
              <textarea class="form-control" rows="3" id="varDescription" name="varDescription" placeholder="ingrese descripcion del menú" autocomplete="off"></textarea>
              <span class="text-danger" id="varDescription_spam"></span>
            </div>
            <div class="form-group">
              <label>Link</label>
              <input type="text" class="form-control" id="varLink" name="varLink" placeholder="controller/action" autocomplete="off">
            </div>
            <div class="form-group">
              <label>Padre</label>
              <select class="form-control" id="varFather_id" name="varFather_id" style="width: 100%;">
              </select>
            </div>
            <div class="form-group">
              <label>Icono</label>
              <input type="text" class="form-control" id="varIcon" name="varIcon" placeholder="Icono" autocomplete="off">
            </div>
            <div class="form-group">
              <label>Orden</label>
              <input type="text" class="form-control" id="intOrden" name="intOrden" placeholder="ingrese el orden del menú" autocomplete="off" required>
              <span class="text-danger" id="intOrden_spam"></span>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary pull-left" data-dismiss="modal">Cerrar</button>
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
      </div>
      <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
  </div>
  <!-- /.modal -->
{{-- !-- Delete Warning Modal -->  --}}
<div class="modal fade" tabindex="-1" data-toggle="modal" data-backdrop="static" data-keyboard="false" id="delete-menu">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
          <span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title w-100 text-center">Ingreso de Nuevo Menú</h4>
      </div>
      <form action="" method="post">
          <div class="modal-body">
              @csrf
              @method('DELETE')
              <h5 class="text-center">Are you sure you want to delete  ?</h5>
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-danger">Yes, Delete Project</button>
          </div>
      </form>
    </div>
  </div>
</div>
  @endsection
  @section('script')
  <script src="{{ asset('assets/admin/datatables.net/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('assets/admin/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  <script src="{{ asset('assets/admin/select2/dist/js/select2.full.min.js') }}"></script>
  <script src="{{ asset('assets/js/util.js') }}"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.14.0/jquery.validate.min.js"></script>
  <script>
    let originalModal = $('#modal-menu').clone();
    $(function () {
      $('#varFather_id').select2({  
        placeholder: "Buscar padre",
        minimumInputLength: 3,  
        language: {
          noResults: function() {
            return "No hay resultado";        
          },
          searching: function() {
            return "Buscando..";
            },
          inputTooShort:function(){
            return 'Ingrese 3 o mas caracteres';
          },
        },
        ajax: { 
            url: BASE_URL+'admin/getparents',
            type: 'post',
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                searchTerm: params.term // search term
              };
            },
            processResults: function (response) {
              return {
                  results: response
              };
            },
            cache: true
          }
      });    
      LaodtableMenu();  
      /* $('#btnagregar').click(function(){
        alert('hiciste click');
      }); */
      /*$("#form-menu").validate({
        rules: {
          varTitle: {
            required: true,
            minlength: 8
          },
          action: "required"
        },
        messages: {
          varTitle: {
            required: "Please enter some data",
            minlength: "Your data must be at least 8 characters"
          },
          action: "Please provide some data"
        }
      });*/
      $('#form-menu').submit(function(event){
        event.preventDefault();
        if(!$(this).valid()){
           return;
        }
        let action=$(this).attr('action');
        $.ajax({
          type: "post",
          url: action,
          data: $('#form-menu').serialize(),
          success: function(data){
            if(data.status==='error'){
              printErrorsinFields(data.message,'form-menu');
            }
            else{
              if(data.status==='success'){
                let messages = $('.messages');
                let successHtml = '<div class="alert alert-success">'+
                  '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                  '<strong><i class="glyphicon glyphicon-ok-sign push-5-r"></</strong> '+ data.message +
                  '</div>';
                  $(messages).html(successHtml);
                   setTimeout(function(){
                      $(messages).html('');
                  }, 5000 ); 
                }
              $('#modal-menu').modal('hide');
              LaodtableMenu();
            }
          },
          error: function (xhr, b, c) {
            //console.log("xhr=" + xhr + " b=" + b + " c=" + c);
            //console.log(xhr.responseJSON.errors);
            console.log('error interno..');
          }
        });
      });
      /*$("#form-editar").submit(function(e) {
        e.preventDefault();
        //console.log('hiciste click en guardas');
        if(!$(this).valid()){
           return;
        }
        //let action=$(this).attr('action');
        //console.log($('#form-editar').serialize());
        $.ajax({
          type: "post",
          url: BASE_URL+'admin/saveeditmenu',
          data: $('#form-editar').serialize(),
          success: function(data){
            if(data.status==='error'){
              printErrorsinFields(data.message,'form-editar');
            }
            else{
              if(data.status==='success'){
                let messages = $('.messages');
                let successHtml = '<div class="alert alert-success">'+
                  '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                  '<strong><i class="glyphicon glyphicon-ok-sign push-5-r"></</strong> '+ data.message +
                  '</div>';
                  $(messages).html(successHtml);
                   setTimeout(function(){
                      $(messages).html('');
                  }, 5000 ); 
                }
              $('#modal-general').modal('hide');
              LaodtableMenu();
            }
          },
          error: function (xhr, b, c) {
            //console.log("xhr=" + xhr + " b=" + b + " c=" + c);
            //console.log(xhr.responseJSON.errors);
            console.log('error interno..');
          }
        });
      });*/
      $('#btnguardar').on('click',function(e){
        e.preventDefault();
        if(!$('#form-editar').valid()){
           return;
        }
        $.ajax({
          type: "post",
          url: BASE_URL+'admin/saveeditmenu',
          data: $('#form-editar').serialize(),
          success: function(data){
            if(data.status==='error'){
              printErrorsinFields(data.message,'form-editar');
            }
            else{
              if(data.status==='success'){
                let messages = $('.messages');
                let successHtml = '<div class="alert alert-success">'+
                  '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                  '<strong><i class="glyphicon glyphicon-ok-sign push-5-r"></</strong> '+ data.message +
                  '</div>';
                  $(messages).html(successHtml);
                   setTimeout(function(){
                      $(messages).html('');
                  }, 5000 ); 
                }
              $('#modal-general').modal('hide');
              LaodtableMenu();
            }
          },
          error: function (xhr, b, c) {
            console.log('error interno..');
          }
        });
      });
      $('#modal-menu').on('hidden.bs.modal', function () {
        $(this).find('form').trigger('reset');
        ClearformModalclose('form-menu');
        /*$('#modal-menu').remove();
        var myClone = originalModal.clone();
        $('body').append(myClone);*/
      });
    });
    
    function LaodtableMenu(){
        $('#table-menu').DataTable({
        paging      : true,
        lengthChange: false,
        searching   : true,
        ordering    : false,
        ajax: {
              type: 'get',
              dataType: 'json',
              url:BASE_URL+ 'admin/getAllMenu',
              dataSrc: "",
              },
        columns:[
            {data:"Padre"         ,class:"textFont text-left"},
            {data:"Hijo"          ,class:"textFont text-left"},
            {data:"varIcon"       ,class:"textFont text-center"},
            {data:"varLink"       ,class:"textFont text-left" , render: function(value, type,full,meta){
                if(value!=null){
                  return  '<a href='+BASE_URL+value+'>'+value+'</a>'
                }
                else{
                  return ''
                }
              }
            },
            {data:"varDescription",class:"textFont text-left"},
            {data:"intOrden"      ,class:"textFont text-center"},
            {data:"bitStatus"     ,class:"textFont text-center"},
            {data:"varMenu_id"    ,class:"textFont text-center", render: function(value, type, full, meta){
              //return '<a href="#" ><img data-link= {{ route('admin.searchmenu_id',['+full.varMenu_id+']) }}  src="{{asset('assets/img/admin/editar.png')}}"  height="18" size="18" onclick="editarmenu(\''+value+'\')" style="cursor:pointer;" "></img></a>'
              //return '<a href="#" ><img data-link= {{ route('admin.searchmenu_id',['+full.varMenu_id+']) }}  src="{{asset('assets/img/admin/editar.png')}}" data-target="#modal-general"  height="18" size="18" style="cursor:pointer;" "></img></a>'
              //return '<button  class="btn btn-primary" data-link="'+BASE_URL+'admin/searchmenu_id/'+value+'" data-toggle="modal" data-target="#modal-general">Editar</button>';
              return '<a href="javascript:void(0)"><img data-link="'+BASE_URL+'admin/searchmenu_id/'+value+'" src="{{asset('assets/img/admin/editar.png')}}" height="18" size="18" data-toggle="modal" data-target="#modal-general"></img></a>'+
                     '<a href="javascript:void(0)"><img data-link="'+BASE_URL+'admin/deletemenu/'+value+'" src="{{asset('assets/img/admin/eliminar.png')}}" height="18" size="18" data-toggle="modal" data-target="#delete-menu"></img></a>'
              }
            }
        ],
        language: {
            url: '{{ asset('js/plugin/datatables/Spanish.txt') }}'
        },
        rowId: 'varMenu_id',
        info        : true,
        autoWidth   : false,
        language: { url: '{{ asset('assets/admin/datatables.net-bs/Spanish.txt') }}'},
        bDestroy: true,
        pageLength: 15
      });
    }
    </script>
  @endsection
  {{-- falta limpiar el css
  falta limpiar select2 al abrir --}}

{{-- SELECT * FROM (
SELECT varMenu_id,varTitle as Padre,'' as Hijo,varIcon,varLink,varDescription,intOrden,bitStatus FROM whcontratistas.t_admin_menu where varFather_id is null
union all
SELECT h.varMenu_id,p.varTitle as Padre,h.varTitle as Hijo,h.varIcon,h.varLink,h.varDescription,h.intOrden,h.bitStatus 
FROM (
	SELECT * FROM whcontratistas.t_admin_menu where varFather_id is null
)p
INNER JOIN (
	SELECT * FROM whcontratistas.t_admin_menu WHERE varFather_id is not null
)h ON p.varMenu_id=h.varFather_id
)H ORDER BY Padre,intOrden --}}