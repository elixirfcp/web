{{-- llamdo a la cabecera  --}}
@extends('layouts.header')
{{-- se puede agregar nuevos estilos en la vista --}}
{{-- <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"> --}}

{{-- contenido principal de la vista --}}
@section('content')
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">
                    <section class="wprt-section">
                        <div class="container">
                            <div class="row">
                               <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>
                                    <h2 class="text-center margin-bottom-10">POLÍTICAS</h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                <div class="col-md-4">
                                    {{-- <blockquote> --}}
                                        <h2>Política de Calidad</h2>
                                        {{-- <small>Someone famous in<cite title='Source Title'>Source Title</cite></small> --}}
                                    {{-- </blockquote> --}}
                                </div>
                               <div class="col-md-8">
                                    <p class='dropcap text-justify'>
                                        Satisfacer a nuestros clientes, facilitando acceso a los servicios de mano de obra prestados con eficacia, eficiencia y efectividad, con responsabilidad social, con un recurso humano competente y buscando la mejora continua de nuestros procesos, WH Contratistas E.I.R.L. y todos sus colaboradores se comprometen a:
                                    </p>
                                    <ul class="wprt-list style-2 accent-color margin-top-30 margin-bottom-25">
                                        <li>Establecer y verificar de manera constante objetivos y metas, para optimizar nuestros procesos y alcanzar la excelencia operacional.</li>
                                        <li>Cumplir con las necesidades de nuestros clientes, alcanzando su satisfacción a través del cumplimiento de sus requisitos.</li>
                                        <li>Brindar servicios de altos estándares y calidad en prevención de riesgos y gestión.</li>
                                    </ul>
                               </div>
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <h2>Política de Medio Ambiente</h2>
                                </div>
                                <div class="col-md-8">
                                        <p class='dropcap text-justify'>
                                            WH Contratistas E.I.R.L. es una empresa orientada a ofrecer servicios de elevada calidad y alto estándar en gestión ambiental, con criterios de competitividad empresarial y sostenibilidad ambiental, asumiendo los siguientes compromisos:
                                        </p>
                                        <ul class="wprt-list style-2 accent-color margin-top-30 margin-bottom-25">
                                            <li>Asegurar la prevención sostenida del cuidado del medio ambiente en los lugares que se efectúan nuestros servicios.</li>
                                            <li>Cumplir la normativa legal vigente y demás requisitos en materia ambiental.</li>
                                            <li>Identificar los aspectos ambientales más significativos para prevenir los impactos al Medio Ambiente.</li>
                                            <li>Mantener una mejora continua en la gestión ambiental y la adopción de las medidas de control más apropiadas para la organización.</li>
                                        </ul>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <h2>Política de Seguridad y Salud</h2>
                                </div>
                                <div class="col-md-8">
                                        <p class='dropcap text-justify'>
                                            WH Contratistas E.I.R.L. es una empresa que para el desarrollo de sus actividades está comprometida a ofrecer servicios de construcción de alta calidad para asegurar una correcta gestión de seguridad y salud en el trabajo para la prevención de los riesgos laborales.Cumple con las disposiciones legales vigentes y aplicables, para alcanzar la mitigación y control de los peligros y evitar riesgos identificados en aras del bienestar integral de nuestros colaboradores para permitir la productividad y el desarrollo sostenible en la prestación de nuestro servicio, asumiendo compromisos como:
                                        </p>
                                        <ul class="wprt-list style-2 accent-color margin-top-30 margin-bottom-25">
                                            <li>Identificar y cumplir los requisitos legales aplicables en Seguridad, Salud Ocupacional. establecidos por el cliente y establecidos por la organización.</li>
                                            <li>Implementación de los medios necesarios para asegurar la integridad física y salud ocupacional de nuestros trabajadores y terceros, en participación de nuestro comité de Seguridad y Salud en Trabajo.</li>
                                            <li>Desarrollar e implementar un Sistema Integrado de Gestión, basado en los lineamientos establecidos en las Normas ISO 9001.</li>
                                        </ul>
                                </div>
                            </div>
                        </div><!-- /.container -->
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- cualquier javascript adicional que se necesite --}}
@section('scripts')
@endsection