{{-- llamdo a la cabecera  --}}
@extends('layouts.header')
{{-- se puede agregar nuevos estilos en la vista --}}
{{-- <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"> --}}

{{-- contenido principal de la vista --}}
@section('content')
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">
                    <section class="wprt-section">
                        <div class="container">
                            <div class="row">
                               <div class="col-md-12">
                                <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>

                                    <h2 class="text-center margin-bottom-10">CULTURA</h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                    <div class="col-md-3">

                                    </div>
                                    <div class="col-md-6">
                                        <p class='dropcap text-justify'>
                                            WH Contratistas E.I.R.L. Promueve una cultura del autocuidado. el trabajo en ambientes seguros y saludables, la búsqueda de la excelencia en la mejora continua y el desempeño eficiente. Estamos convencidos que la vida es el bien más preciado y el cuidado del medio ambiente es parte esencial para la organización.Es por ello, que trabajamos en el desarrollo de una disciplina de trabajo comprometida con el mantenimiento de estándares de calidad elevados, con el cuidado de la salud y la integridad física de nuestros colaboradores, y con la preservación del medio ambiente.
                                        </p>
                                    </div>
                                    <div class="col-md-3">
                                    </div>
                                </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- cualquier javascript adicional que se necesite --}}
@section('scripts')
@endsection