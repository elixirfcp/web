{{-- llamdo a la cabecera  --}}
@extends('layouts.header')
{{-- se puede agregar nuevos estilos en la vista --}}
{{-- <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"> --}}

{{-- contenido principal de la vista --}}
@section('content')
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">
                    <section class="wprt-section">
                        <div class="container">
                            <div class="row">
                               <div class="col-md-12">
                                <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>

                                    <h2 class="text-center margin-bottom-10">MISIÓN</h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                
                                
                                <div class="col-md-4">
                                    <p class='dropcap text-justify'>
                                        <span class='first-letter'>S</span>
                                        omos una empresa que brinda a sus clientes servicios integrados de construcción basados en la excelencia para superar sus espectativas en un entorno de trabajo seguro y comprometidos en contribuir con el desarrollo del país.
                                    </p>
                                </div>

                                <div class="col-md-4">
                                    {{-- <p class='dropcap-contrast text-justify'>
                                        <span class='first-letter'>L</span>
                                        orem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed massa quis elit sagittis tempor. Ut rutrum sem vel augue rhoncus vestibulum. Cras aliquam accumsan lorem, ac cursus orci molestie eget. Ut nec ultricies augue. Vestibulum lobortis enim felis.
                                    </p>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div> --}}
                                </div>

                                <div class="col-md-4">
                                    <blockquote>
                                        <p>Ser reconocidos como una empresa lider del sector, por ser la mas segura y eficaz del medio</p>
                                        <small>Someone famous in<cite title='Source Title'>Source Title</cite></small>
                                    </blockquote>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div>
                                </div>
                            </div><!-- /.row -->
                            <div class="row">
                                <div class="col-md-12"> 
                                     <h2 class="text-center margin-bottom-10">VISIÓN</h2>
                                     <div class="wprt-lines style-2 custom-1">
                                         <div class="line-1"></div>
                                     </div>
                                     <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                 
                                 
                                 <div class="col-md-4">
                                     <p class='dropcap text-justify'>
                                         <span class='first-letter'>S</span>
                                         er reconocidos como una empresa lider del sector, por ser la mas segura y eficaz del medio
                                     </p>
                                 </div>
 
                                 <div class="col-md-4">
                                     {{-- <p class='dropcap-contrast text-justify'>
                                         <span class='first-letter'>L</span>
                                         orem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed massa quis elit sagittis tempor. Ut rutrum sem vel augue rhoncus vestibulum. Cras aliquam accumsan lorem, ac cursus orci molestie eget. Ut nec ultricies augue. Vestibulum lobortis enim felis.
                                     </p>
 
                                     <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div> --}}
                                 </div>
 
                                 <div class="col-md-4">
                                     <blockquote>
                                         <p>Ser reconocidos como una empresa lider del sector, por ser la mas segura y eficaz del medio</p>
                                         <small>Someone famous in<cite title='Source Title'>Source Title</cite></small>
                                     </blockquote>
 
                                     <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                 </div>
                                 </div>
                             </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- cualquier javascript adicional que se necesite --}}
@section('scripts')
@endsection