{{-- llamdo a la cabecera  --}}
@extends('layouts.header')
{{-- se puede agregar nuevos estilos en la vista --}}
{{-- <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"> --}}

{{-- contenido principal de la vista --}}
@section('content')
<!-- Main Content -->
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">
                    <section class="wprt-section">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>
                                    {{-- <div id="gmap" class="wprt-gmap"></div> --}}
                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                </div><!-- /.col-md-12 -->

                                <div class="col-md-4">
                                    <div class="wprt-information">
                                        <h4 class="margin-bottom-15">LLAMENOS</h4>
                                        <p>Construir no es solo un trabajo. En Construction Company, es nuestra pasión. Con cada proyecto que emprendemos, ponemos el listón alto y proporcionamos a las mejores personas de la industria, con un verdadero amor por lo que hacemos para hacer realidad la visión de nuestros Clientes</p>
                                        <ul class="style-2">
                                            <li class="address">
                                                <span class="hl"><b>Dirección:</b></span>
                                                <span class="text">Av. Los Cedros Mz J Lt 13B, Villa Maria del Triunfo, Lima Perú</span>
                                            </li>
                                            <li class="phone">
                                                <span class="hl"><b>Celular:</b></span> 
                                                <span class="text">+51 943-205-078</span>
                                            </li>
                                            <li class="email">
                                                <span class="hl"><b>E-mail:</b></span>
                                                <span class="text">contactos@whcontratistas.com</span>
                                            </li>
                                        </ul>
                                    </div><!-- /.wprt-information -->

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->

			                    <div class="col-md-8">
			                    	<h4 class="margin-bottom-15">PÓNGASE EN CONTACTO CON NOSOTROS</h4>
									<form action="#" method="post" class="contact-form wpcf7-form">
										<div class="wprt-contact-form-1">
											<span class="wpcf7-form-control-wrap name">
												<input type="text" tabindex="1" id="name" name="name" value="" class="wpcf7-form-control" placeholder="Nombres Completos *" required autocomplete="off">
											</span>
											<span class="wpcf7-form-control-wrap email">
												<input type="email" tabindex="2" id="email" name="email" value="" class="wpcf7-form-control" placeholder="E-mail *" required autocomplete="off">
											</span>
											<span class="wpcf7-form-control-wrap phone">
												<input type="text" tabindex="3" id="phone" name="phone" value="" class="wpcf7-form-control" placeholder="Número de Celular" autocomplete="off">
											</span>
											<span class="wpcf7-form-control-wrap subject">
												<input type="text" tabindex="4" id="subject" name="subject" value="" class="wpcf7-form-control" placeholder="Asunto *" required autocomplete="off">
											</span>
											<span class="wpcf7-form-control-wrap message">
												<textarea name="message" tabindex="5" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" placeholder="Mensaje" required autocomplete="off"></textarea>
											</span>
											<div class="wrap-submit">
												<input type="submit" value="ENVIAR MENSAJE" class="submit wpcf7-form-control wpcf7-submit" id="submit" name="submit">
											</div>
										</div>
									</form>
			                    </div><!-- /.col-md-8 -->

                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- cualquier javascript adicional que se necesite --}}
@section('scripts')
@endsection