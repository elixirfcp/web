{{-- llamdo a la cabecera  --}}
@extends('layouts.header')
{{-- se puede agregar nuevos estilos en la vista --}}
{{-- <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet" type="text/css"> --}}

{{-- contenido principal de la vista --}}
@section('content')
<div id="main-content" class="site-main clearfix">
    <div id="content-wrap">
        <div id="site-content" class="site-content clearfix">
            <div id="inner-content" class="inner-content-wrap">
                <div class="page-content">
                    <section class="wprt-section">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>
                                    <h2 class="text-center margin-bottom-10">LO QUE OFRCEMOS</h2>
                                    <div class="wprt-lines style-2 custom-1">
                                        <div class="line-1"></div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="25" data-mobi="25" data-smobi="25"></div>

                                    <p class="wprt-subtitle">Cras id justo eget sapien scelerisque lacinia non a eros. In a volutpat magna. Vivamus pretium urna at condimentum porta. Phasellus mollis mauris dolor, a tincidunt mauris fringilla.</p>

                                    <div class="wprt-spacer" data-desktop="50" data-mobi="40" data-smobi="40"></div>
                                </div><!-- /.col-md-12 -->
                                        
                                <div class="col-md-4">
                                    <div class="wprt-icon-box accent-background rounded icon-effect-3 icon-right">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-o-roof"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18">Metal Roofing</h3>
                                            <p>Vestibulum eu libero volutpat, portas quam acc, tempus sem. Donec sodales quam id lorem lobortis, interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="22" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box accent-background rounded icon-effect-3 icon-right">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-o-light-bulb"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18">Electrical Systems</h3>
                                            <p>Vestibulum eu libero volutpat, portas quam acc, tempus sem. Donec sodales quam id lorem lobortis, vitae nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="22" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box accent-background rounded icon-effect-3 icon-right">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-o-paint-roller"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18">Tiling and Painting</h3>
                                            <p>Vestibulum eu libero volutpat, portas quam acc, tempus sem. Donec sodales id lorem lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="20" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                	<div class="text-center">
										<img src="{{asset('assets/img/worker.png')}}" alt="image" />
									</div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box accent-background rounded icon-effect-3 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-o-glass-wall"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18">Laminate Flooring</h3>
                                            <p>Vestibulum eu libero volutpat, portas quam acc, tempus sem. Donec quam id lorem lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="22" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box accent-background rounded icon-effect-3 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-o-plumbering"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18">Plumbilg Services</h3>
                                            <p>Vestibulum eu libero volutpat, portas quam acc, tempus sem. Donec sodales quam id lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="20" data-mobi="30" data-smobi="30"></div>

                                    <div class="wprt-icon-box accent-background rounded icon-effect-3 icon-left">
                                        <div class="icon-wrap">
                                            <span class="dd-icon icon-o-drill-1"></span>
                                        </div>
                                        <div class="content-wrap">
                                            <h3 class="dd-title font-size-18">Restoration Services</h3>
                                            <p>Vestibulum eu libero volutpat, portas quam, tempus sem. Donec sodales quam id lorem lobortis, vitae interdum nisl.</p>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="30" data-smobi="30"></div>
                                </div><!-- /.col-md-4 -->
                            </div><!-- /.row -->
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->

                                <div class="col-md-4">

                                    <div class="service-item clearfix">
                                        <div class="thumb"><img src="{{asset('assets/img/services/4.jpg')}}" alt="image" /></div>
                                        <div class="service-item-wrap">
                                            <h3 class="title font-size-18"><a href="#">Construction Consultant</a></h3>
                                            <p class="desc">Nulla iaculis turpis in nibh aliquam maximus. In dignissim arcu vel diam scelerisque, pretium urna </p>
                                            <a href="#" class="wprt-button small rounded-3px">READ MORE</a>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="55" data-mobi="40" data-smobi="40"></div>

                                    <div class="service-item clearfix">
                                        <div class="thumb"><img src="{{asset('assets/img/services/1.jpg')}}" alt="image" /></div>
                                        <div class="service-item-wrap">
                                            <h3 class="title font-size-18"><a href="#">Design & Construction</a></h3>
                                            <p class="desc">Nulla iaculis turpis in nibh aliquam maximus. In dignissim arcu vel diam scelerisque, pretium urna </p>
                                            <a href="#" class="wprt-button small rounded-3px">READ MORE</a>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="service-item clearfix">
                                        <div class="thumb"><img src="{{asset('assets/img/services/5.jpg')}}" alt="image" /></div>
                                        <div class="service-item-wrap">
                                            <h3 class="title font-size-18"><a href="#">Resconstruction Services</a></h3>
                                            <p class="desc">Nulla iaculis turpis in nibh aliquam maximus. In dignissim arcu vel diam scelerisque, pretium urna </p>
                                            <a href="#" class="wprt-button small rounded-3px">READ MORE</a>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="55" data-mobi="40" data-smobi="40"></div>

                                    <div class="service-item clearfix">
                                        <div class="thumb"><img src="{{asset('assets/img/services/2.jpg')}}" alt="image" /></div>
                                        <div class="service-item-wrap">
                                            <h3 class="title font-size-18"><a href="#">The Green Building</a></h3>
                                            <p class="desc">Nulla iaculis turpis in nibh aliquam maximus. In dignissim arcu vel diam scelerisque, pretium urna </p>
                                            <a href="#" class="wprt-button small rounded-3px">READ MORE</a>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="0" data-mobi="40" data-smobi="40"></div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-4">
                                    <div class="service-item clearfix">
                                        <div class="thumb"><img src="{{asset('assets/img/services/6.jpg')}}" alt="image" /></div>
                                        <div class="service-item-wrap">
                                            <h3 class="title font-size-18"><a href="#">Architectural Design</a></h3>
                                            <p class="desc">Nulla iaculis turpis in nibh aliquam maximus. In dignissim arcu vel diam scelerisque, pretium urna </p>
                                            <a href="#" class="wprt-button small rounded-3px">READ MORE</a>
                                        </div>
                                    </div>

                                    <div class="wprt-spacer" data-desktop="55" data-mobi="40" data-smobi="40"></div>
                                    
                                    <div class="service-item clearfix">
                                        <div class="thumb"><img src="{{asset('assets/img/services/3.jpg')}}" alt="image" /></div>
                                        <div class="service-item-wrap">
                                            <h3 class="title font-size-18"><a href="#">House Renovation</a></h3>
                                            <p class="desc">Nulla iaculis turpis in nibh aliquam maximus. In dignissim arcu vel diam scelerisque, pretium urna </p>
                                            <a href="#" class="wprt-button small rounded-3px">READ MORE</a>
                                        </div>
                                    </div>
                                </div><!-- /.col-md-4 -->

                                <div class="col-md-12">
                                    <div class="wprt-spacer" data-desktop="80" data-mobi="60" data-smobi="60"></div>
                                </div><!-- /.col-md-12 -->
                            </div><!-- /.row -->
                        </div><!-- /.container -->
                    </section>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
{{-- cualquier javascript adicional que se necesite --}}
@section('scripts')
@endsection