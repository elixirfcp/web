<!-- Footer -->
<footer id="footer">
    <div id="footer-widgets" class="container style-5">
        <div class="row">
            <div class="col-md-4">
                <div class="widget widget_text">
                    <h2 class="widget-title"><span>SOBRE NOSOTROS</span></h2>
                    <div class="textwidget">
                        <img src="{{asset('assets/img/logo.png')}}"  width="200" height="30" alt="image" class="margin-top-10 margin-bottom-25" />
                        <p>Construir no es solo un trabajo. En Construction Company, es nuestra pasión. Con cada proyecto que emprendemos, ponemos el listón alto y proporcionamos a las mejores personas de la industria, con un verdadero amor por lo que hacemos para hacer realidad la visión de nuestros Clientes</p>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="widget widget_links">
                    <h2 class="widget-title"><span>LINKS DE COMPANIAS</span></h2>
                    <ul class="wprt-links clearfix col2">
                        <li class="style-2"><a href="#">History</a></li>
                        <li class="style-2"><a href="#">Services</a></li>
                        <li class="style-2"><a href="#">Team & Awards</a></li>
                        <li class="style-2"><a href="#">Delivery Methods</a></li>
                        <li class="style-2"><a href="#">Community</a></li>
                        <li class="style-2"><a href="#">Safety</a></li>
                        <li class="style-2"><a href="#">News & Events</a></li>
                        <li class="style-2"><a href="#">Sustainability</a></li>
                        <li class="style-2"><a href="#">FAQ</a></li>
                        <li class="style-2"><a href="#">Portfolio</a></li>
                        <li class="style-2"><a href="#">Contact Us</a></li>
                        <li class="style-2"><a href="#">Shop</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-4">
                <div class="widget widget_information">
                    <h2 class="widget-title"><span>INFORMACIÓN DE CONTACTO</span></h2>
                    <ul class="style-2">
                        <li class="address clearfix">
                            <span class="hl">Dirección:</span>
                            <span class="text">Av. Los Cedros Mz J Lt 13B, Villa Maria del Triunfo, Lima Perú</span>
                        </li>
                        <li class="phone clearfix">
                            <span class="hl">Celular:</span> 
                            <span class="text">+51 943-205-078</span>
                        </li>
                        <li class="email clearfix">
                            <span class="hl">E-mail:</span>
                            <span class="text">contactos@whcontratistas.com</span>
                        </li>
                    </ul>
                </div>

                <div class="widget widget_spacer">
                    <div class="wprt-spacer clearfix" data-desktop="10" data-mobi="10" data-smobi="10"></div>
                </div>

                <div class="widget widget_socials">
                    <div class="socials">
                        <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-google-plus"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-pinterest"></i></a>
                        <a target="_blank" href="#"><i class="fa fa-dribbble"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>

</div><!-- /#page -->
</div><!-- /#wrapper -->

<a id="scroll-top"></a>

<!-- Javascript -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/animsition.js') }}"></script>
<script src="{{ asset('assets/js/plugins.js') }}"></script>
<script src="{{ asset('assets/js/countTo.js') }}"></script>
<script src="{{ asset('assets/js/flexslider.js') }}"></script>
<script src="{{ asset('assets/js/owlCarousel.js') }}"></script>
<script src="{{ asset('assets/js/cube.portfolio.js') }}"></script>
<script src="{{ asset('assets/js/main.js') }}"></script>


<!-- Revolution Slider -->
<script src="{{ asset('assets/slider/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/jquery.themepunch.revolution.min.js') }}"></script>
<script src="{{ asset('assets/js/rev-slider.js') }}"></script>

<!-- Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading -->  
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('assets/slider/js/extensions/revolution.extension.video.min.js') }}"></script>

</body>
</html>