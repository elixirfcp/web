<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\administration\Menu;
use App\Util\Util;
use Exception;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;

class AdministrationController extends Controller{
    public function __construct(){
    
    }
    public function login(){
        return view('administration.login');
    }
    public function principal(Request $request){
        return view('administration.index');
    }
    public function menu(){
        //dd(url()->current());
        $data['menu']=Menu::all();
        return view('administration.menu',$data);
    }
    public function createmenu(Request $entrada){
        $result = ['status'=>'success', 'message'=>'Se guardó el nuevo registro...'];
        $menu= new Menu();
        try {
            $entrada->validate([
                'varTitle' => 'required|min:4|max:100',
                'intOrden' => 'required',
                'varDescription'=>'required',
            ],[
                'varTitle.required' => ' El titúlo es requerido.',
                'varTitle.min' => ' El minimo de caracteres es 4.',
                'varTitle.max' => ' El máximo de caracteres es 100. ',
                'intOrden.required' => ' El orden es requerido.',
                'varDescription.required'=>'La descripción des requerida'
            ]);        
            $menu->varMenu_id=Util::com_create_guid();
            $model  = $entrada->all();
            $menu->fill($model);
            $menu->save();
            }catch (ValidationException $ex){
                 $result=['status'=>'error','message'=>$ex->errors()];
            } 
            catch (Exception $e) {
                $result =['status'=>'error','message'=>'Ocurrió un problema al realizar registro... '.$e->getMessage()];
            }
            return $result;
        
    }
    public function getparents(Request $entrada){
        if(isset($entrada->searchTerm)){
            $resultado=Menu::where('bitStatus','=',1)->where('varTitle','like',$entrada->searchTerm.'%')->get();
            if($resultado->isNotEmpty()){
                foreach ($resultado as $menu) {
                    $json[]= ['id'=>$menu->varMenu_id, 'text'=>$menu->varTitle]; 
                }
            }
            else{
                $json = [];    
            }
        }else{
            $json = [];
        }
        echo json_encode($json);
    }
    public function getAllMenu(){
       return json_encode(DB::select("CALL GetAllMenu()"),true);
    }
    public function searchmenu_id($menu_id){
        $data['menu']=Menu::find($menu_id);
        if($data['menu']['varFather_id']!=null){
            $data['padre']=Menu::find($data['menu']['varFather_id']);
        }
        return view('administration.editmenu',$data);
    }
    public function saveeditmenu(Request $entrada){
        $result = ['status'=>'success', 'message'=>'Se actualizó el registro...'];
        $model=Menu::find($entrada->varMenu_id);
        $menu=$entrada->all();
        try {
                $model->fill($menu);
                $model->save();
            }
            catch (Exception $e) {
                $result =['status'=>'error','message'=>'Ocurrió un problema al realizar registro... '.$e->getMessage()];
            }
        return $result;
    }
    public function deletemenu(Request $entrada){
        $result = ['status'=>'success', 'message'=>'Se eliminó el registro...'];
        $model=Menu::find($entrada->varMenu_id);
        $model->bitStatus=0;
        $model->save();
        return $result;
    }
    public function aves(){
        return view('administration.aves');       
    }
    //esto es un cambio
    public function testing(){
        return 'test';
    }
}