<?php

namespace App\Models\administration;

use Illuminate\Database\Eloquent\Model;



class Menu extends Model{
    //use Auditorias;
    protected $table = 't_admin_menu';
    protected $primaryKey='varMenu_id';
    protected $keyType = 'string';
    protected $with = [];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'varMenu_id', 'varFather_id','varLink', 'varTitle', 'varIcon', 'varDescription', 'intOrden', 'bitStatus'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function padre(){
        return $this->belongsTo('App\Models\administration\Menu', 'varFather_id');
    }

    public function hijos(){
        return $this->hasMany('App\Models\administration\Menu', 'varFather_id');
    }

    /*public function roles()
    {
        return $this->belongsToMany('App\Admin\Models\Rol', 'T_GNRL_MENU_X_ROL', 'menu_principal_id', 'rol_id');
    }

    public function scopePorRol(Builder $query, $rolId)
    {
        $menuIds =  DB::table('T_GNRL_MENU_X_ROL')->where('rol_id', $rolId)->pluck('menu_principal_id');
        return $query->findMany($menuIds);
    }

    public function setStatusAttribute($value)
    {
        $this->attributes['status'] =  (bool) $value;
    }

    public function getStatusAttribute($value)
    {
       return (bool) $value;
    }

    public static function boot()
    {
        self::auditarUsuarios();
    }*/
}