<?php

namespace App\Util;
use Illuminate\Support\Facades\Auth;
use App\Models\administration\Menu;

class Util{
    public function __construct(){

    }
   public static function com_create_guid(){
        if (function_exists('com_create_guid')){
            return com_create_guid();
        }else{
            mt_srand((double)microtime()*10000);
            $charid = md5(uniqid(rand(), true));
            $hyphen = chr(45);
            $uuid = substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12);
            return $uuid;
        }
    }
    public static function prepararmenu($pagina_seleccionada){
        //$rolId = Auth::user()->rol->first()->id;
        return self::generarmenu(Menu::all()->whereNull('varFather_id'),null,null, $pagina_seleccionada);
    }
    public static function generarmenu($menus, $parentid = null, $indice = null, $pagina_seleccionada = null){
        $response = '';
        $li_item  = '';
        $num_orden = [];
        /* <li class="active treeview menu-open">
        <a href="#">
          <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li class="active"><a href="{{ asset('admin/menu')}}"><i class="fa fa-circle-o"></i> Menú</a></li>
          <li><a href="index2.html"><i class="fa fa-circle-o"></i> Dashboard v2</a></li>
        </ul>
      </li> */
        $lista_padres='';
        $lista_hijos='';
        $menuhijoactivo="";
        $menupadreactivo="treeview";
        foreach ($menus as $menu) {
            if($menu->hijos()->count()>0){
                $lista_hijos='<ul class="treeview-menu">';
                foreach($menu->hijos()->get() as $item){
                    $link=asset($item->varLink);
                    if($link==$pagina_seleccionada){
                        $menuhijoactivo="active";
                        $menupadreactivo="active treeview menu-open";
                    }
                    else{
                        $menuhijoactivo="";
                    }
                    $lista_hijos.='<li class="'.$menuhijoactivo.'"><a href="'.asset($item->varLink).'"><i class="fa fa-circle-o"></i>'.$item->varTitle.'</a></li>';
                }
                $lista_padres='<li class="'.$menupadreactivo.'"> <a href="#"><i class="fa fa-dashboard"></i><span>'.$menu->varTitle.'</span> <span class="pull-right-container"> <i class="fa fa-angle-left pull-right"></i></span> </a>';
                $lista_hijos.='</ul>';
                $lista_padres.=$lista_hijos;
            }
            $lista_padres.='</li>'; 
            $response .=$lista_padres;
            unset($lista_padres);
            unset($lista_hijos);
            $menupadreactivo="treeview";
        }
             
            

            /*if(!$menu->bitStatus) {
                continue;
            }
            if ($menu->varFather_id != $parentid) {
                continue;
            }
            $esPadre = $menu->hijos->count() > 0;
            $link = $esPadre ? '#' :  asset($menu->varLink);

            if($link == $pag_seleccionada) {
                $li_item = '  <li class="active" >  ';
            }else{
                $li_item = '  <li class="">  ';
            }
            $a_titulo = "<a href='{$link}' >";
            if(isset($menu->varIcon)) {
                $i_icono  = "<i class='fa-fw {$menu->varIcon}' ></i>";
            } else {
                $i_icono = '';
            }
            if($menu->varFather_id === null) {
                $titulo   = " <span class='menu-item-parent'>{$menu->varTitle}</span>";
            } else {
                $titulo = self::generarIndice($menu, $indice) .'<span class="menu-titulo">'. $menu->varTitle.'</span>';
            }
            $a_titulo .= $i_icono . $titulo . "</a>"; // . $i_popover
            // JUNTANDO EL MENU CREADO
            $li_item .= $a_titulo;
            if($esPadre) {
                $li_item .= '  <ul>' . self::generarmenu($menus->sortBy('orden'), $menu->varMenu_id, $menu->indice, $pag_seleccionada) . '</ul>  ';
            }
            $response .= $li_item . '</li>  ';*/
        
        //dump($response);
        return $response;
    }
    protected static function generarhijos($menupadre){
        
    }

    private static function generarIndice($menu, $indice){
        if(in_array($menu->padre_id, [5,7,8])) {
            $indice         .= self::num_romanos($menu->intOrden);
            $menu->indice   = $menu->intOrden.'.';
        } else {
            $indice         .= $menu->intOrden;
            $menu->indice   = $indice.'.';
        }

        return '<span class="menu-indice">'.$indice.'.</span>';
        //return $indice.'. ';
    }

   private static function num_romanos($integer, $upcase = true){
        $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
        $return = '';
        while($integer > 0) {
            foreach($table as $rom=>$arb) {
                if($integer >= $arb) {
                    $integer -= $arb;
                    $return .= $rom;
                    break;
                }
            }
        }
        return $return;
    }
 
}