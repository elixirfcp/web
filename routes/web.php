<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AdministrationController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/
Route::get('/', [HomeController::class,'index'])->name('inicio');
Route::group(['prefix' => 'web'], function() {
});
Route::group(['prefix' => 'admin'], function() {
    Route::get('login', [AdministrationController::class, 'login'])->name('admin.login');
    Route::post('principal', 'AdministrationController@principal')->name('admin.principal');
    Route::get('menu', 'AdministrationController@menu')->name('admin.menu');
    Route::post('createmenu', 'AdministrationController@createmenu')->name('admin.createmenu');
    Route::post('getparents', 'AdministrationController@getparents')->name('admin.getparents');
    Route::get('getAllMenu', 'AdministrationController@getAllMenu')->name('admin.getAllMenu');
    Route::get('searchmenu_id/{menu_id}', 'AdministrationController@searchmenu_id')->name('admin.searchmenu_id');
    Route::post('saveeditmenu','AdministrationController@saveeditmenu')->name('admin.saveeditmenu');
    Route::post('deletemenu','AdministrationController@deletemenu')->name('admin.deletemenu');
    Route::get('aves','AdministrationController@aves')->name('admin.aves');
});
Route::group(['prefix'=>'web'], function(){
    Route::get('contacto','WebController@contacto')->name('web.contacto');
    Route::get('nosotros','WebController@nosotros')->name('web.nosotros');
    Route::get('misionvision','WebController@misionvision')->name('web.misionvision');
    Route::get('valores','WebController@valores')->name('web.valores');
    Route::get('cultura','WebController@cultura')->name('web.cultura');
    Route::get('politicas','WebController@politicas')->name('web.politicas');
    Route::get('servicios','WebController@servicios')->name('web.servicios');
});