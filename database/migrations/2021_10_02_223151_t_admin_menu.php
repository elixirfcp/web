<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TAdminMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('t_admin_menu', function (Blueprint $table) {
            $table->string('varMenu_id',40)->primary();
            $table->string('varFather_id',40)->nullable();
            $table->string('varTitle',100);
            $table->string('varIcon',20)->nullable();
            $table->string('varLink',200)->nullable();
            $table->text('varDescription')->nullable();
            $table->unsignedInteger('intOrden');
            $table->boolean('bitStatus')->default(1);
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('deleted_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(){
        Schema::dropIfExists('t_admin_menu');
    }
}
